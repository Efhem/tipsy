//
//  CalculateBrain.swift
//  Tipsy
//
//  Created by Femi Adegbite on 12/04/2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation


struct CalculateBrain {
    
    var tip: Double = 0.10
    var split: Int = 2
    var bill = 0.0
    
    mutating func setTip( _ selectedTip: String){
        let buttonTitleMinusPercentSign =  String(selectedTip.dropLast())
        let buttonTitleAsANumber = Double(buttonTitleMinusPercentSign)!
        self.tip = buttonTitleAsANumber / 100
    }
    
    mutating func setSlit(_ selectedSlit: Int ){
        split = selectedSlit
    }
    
    mutating func setBill(_ bill: Double ){
        self.bill = bill
    }
    
    func getTotalBill() -> Double {
        return bill * (1 + tip) / Double(split)
    }
    
    
}
