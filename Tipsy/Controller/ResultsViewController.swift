//
//  ResultsViewController.swift
//  Tipsy
//
//  Created by Femi Adegbite on 12/04/2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController {

    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    var result: Double = 0.0
    var numberOfPeople: Int = 2
    var tip: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        totalLabel.text = String(format: "%.2f", result)
        settingsLabel.text = "Split between \(numberOfPeople) people, with \(Int(tip * 100))% tip."
    }

    @IBAction func recalculatPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
