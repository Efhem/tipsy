//
//  CalculatorViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {


    @IBOutlet weak var billTextField: UITextField!
    
    @IBOutlet weak var zeroPctButton: UIButton!
    @IBOutlet weak var tenPctButton: UIButton!
    @IBOutlet weak var twentyPctButton: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!
    

    var calculateBrain = CalculateBrain()

    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        calculateBrain.split = Int(sender.value)
        
        splitNumberLabel.text = String(calculateBrain.split)
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        
        let bill  = billTextField.text!
        
        if  bill != "" {
            calculateBrain.bill = Double(bill)!
            self.performSegue(withIdentifier: "action_calculatedVC_to_resultVC", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let identifier  = segue.identifier
        if identifier == "action_calculatedVC_to_resultVC" {
            let destination  = segue.destination as! ResultsViewController
            destination.result = calculateBrain.getTotalBill()
            destination.numberOfPeople = calculateBrain.split
            destination.tip = calculateBrain.tip
        }
        
    }
    
    
    @IBAction func tipChangeld(_ sender: UIButton) {
        
        zeroPctButton.isSelected = false
        tenPctButton.isSelected = false
        twentyPctButton.isSelected = false
        sender.isSelected = true
        
        billTextField.endEditing(true)
        
        let buttonTitle = sender.currentTitle!
        calculateBrain.setTip(buttonTitle)
       
    }

}

